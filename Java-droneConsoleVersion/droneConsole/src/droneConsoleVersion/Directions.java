package droneConsoleVersion;

public class Directions {
	public String directionX = "";
	public String directionY = "";

	public Directions(String x, String y) { // directions constructor
		directionX = x;
		directionY = y;
	}

	public String getDirectionX() { // getter rows
		return directionX;
	}

	public String getDirectionY() { // getter cols
		return directionY;
	}

	public void setDirectionX(String directionX) { // setter rows
		this.directionX = directionX;
	}

	public void setDirectionY(String directionY) { // setter cols
		this.directionY = directionY;
	}

	@Override
	public String toString() {
		return directionX + directionY; // return directions
	}
}
