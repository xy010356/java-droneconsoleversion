package droneConsoleVersion;

public class Drone {

	private int x, y, droneID; // positions and drone ID
	private static int droneNumber = 0;
	public Directions movement = new Directions("", "");

	public Drone(int posx, int posy) {
		x = posx;
		y = posy;
		droneID = droneNumber++;
	}

	public Drone(int posx, int posy, int id) {
		this(posx, posy);
		droneNumber = ++id;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int getID() {
		return droneID;
	}

	public void setXY(int x, int y) {
		this.x = x;
		this.y = y;
	}

//	public void stringSplitter() {
//
//	    String newStr = "";
//	    for(int i = 0; i<str.length(); i++) {
//	            char character = str.charAt(i);
//	            if(!String.valueOf(character).contains(" ")) {
//	                newStr += character + ", ";
//	            }
//			}
//			newStr = newStr.substring(0, newStr.length() - 2);
//			str = newStr;
//	}
//	

	@Override
	public String toString() {
		return "Drone " + droneID + " at" + " " + x + ", " + y;
	}

}
